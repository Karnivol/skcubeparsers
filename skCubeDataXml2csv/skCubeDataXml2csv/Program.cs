﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace skCubeDataXml2csv
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Count() < 1)
            {
                Console.WriteLine("Provide folder path in argument and filter argument.");
                Console.WriteLine("Example: skCubeDataXml2csv.exe d:\\xml_files COM");

                return;
            }
            else if (args.Count() == 1)
            {
                Console.WriteLine("Provide filter argument.");
                Console.WriteLine("Example: skCubeDataXml2csv.exe d:\\xml_files COM");
                return;
            }

            File.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "out.csv"));

            string filterby = args[1];

            XmlDocument docu = new XmlDocument();
            StringBuilder csv = new StringBuilder();

            string dirPath = args.First();
            if (!Directory.Exists(args.First()))
            {
                Console.WriteLine("Invalid directory.");
                return;
            }

            var dirs = Directory.GetDirectories(args.First());
            if (dirs.Count() == 0)
            {
                var files = Directory.GetFiles(dirPath).Where(i => i.Contains(filterby)).ToList();
                if (files.Count() == 0)
                {
                    Console.WriteLine("No files found. Check filter argument.");
                    return;
                }

                docu.Load(files.First());
                csv.Append("Timestamp");
                for (int i = 0; i < docu.ChildNodes[0].ChildNodes[1].ChildNodes.Count; i++)
                {
                    var element = docu.ChildNodes[0].ChildNodes[1].ChildNodes[i] as XmlElement;
                    csv.Append("," + element.Attributes[1].Value.Replace(' ', '_'));
                }

                csv.Append("\n");

                if (files.Count() != 0)
                {
                    foreach (var file in files)
                    {
                        try
                        {
                            docu.Load(file);
                            
                            csv.Append(docu.ChildNodes[0].ChildNodes[0].InnerText);
                            for (int i = 0; i < docu.ChildNodes[0].ChildNodes[1].ChildNodes.Count; i++)
                            {
                                var element = docu.ChildNodes[0].ChildNodes[1].ChildNodes[i] as XmlElement;
                                csv.Append("," + element.InnerText.Trim());
                            }

                            csv.Append("\n");
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
            else
            {
                List<string> files = new List<string>();
                for (int i = 0; i < dirs.Count(); i++)
                {
                    files = Directory.GetFiles(dirs[i]).Where(it => it.Contains(filterby)).ToList();
                    if (files.Count != 0)
                    {
                        break;
                    }
                }

                if (files.Count == 0)
                {
                    Console.WriteLine("No files found. Check filter argument.");
                    return;
                }

                docu.Load(files.First());
                csv.Append("Timestamp");
                for (int i = 0; i < docu.ChildNodes[0].ChildNodes[1].ChildNodes.Count; i++)
                {
                    var element = docu.ChildNodes[0].ChildNodes[1].ChildNodes[i] as XmlElement;
                    csv.Append("," + element.Attributes[1].Value.Replace(' ', '_'));
                }

                csv.Append("\n");

                foreach (var dir in dirs)
                {
                    files = Directory.GetFiles(dir).Where(i => i.Contains(filterby)).ToList();

                    foreach (var file in files)
                    {
                        try
                        {
                            docu.Load(file);

                            csv.Append(docu.ChildNodes[0].ChildNodes[0].InnerText);
                            for (int i = 0; i < docu.ChildNodes[0].ChildNodes[1].ChildNodes.Count; i++)
                            {
                                var element = docu.ChildNodes[0].ChildNodes[1].ChildNodes[i] as XmlElement;
                                csv.Append("," + element.InnerText.Trim());
                            }

                            csv.Append("\n");
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            File.AppendAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "out.csv"), csv.ToString());
        }
    }
}
