﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace satnogs2csv
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Count() == 0)
            {
                Console.Error.WriteLine("Please provide directory path and type of data you want to parse.");
                Console.Error.WriteLine("Example: satnogs2csv.exe d:\\satnogs_file.csv COM");
                return;
            }
            else if (args.Count() == 1)
            {
                Console.Error.WriteLine("Please provide directory path and type of data you want to parse.");
                Console.Error.WriteLine("Example: satnogs2csv.exe d:\\satnogs_file.csv COM");
                return;
            }

            var csvFile = new StringBuilder();
            ushort identificator = 0;
            Func<string, string> operation;
            switch (args[1])
            {
                case "CDHS":
                    identificator = 1;
                    operation = GetCdhsLine;
                    csvFile.Append("DB_time,Antennas_fired,Antenna_pair_GPIO_state1,Antenna_pair_GPIO_state2,Patch_antenna_state,Antenna_firing_attempts,Antennas_fired_inverted,Num_reboots_current_CPU,Time,Bootlog_OPER_img_in_FLASH_OK_,Bootlog_OPER_img_in_FLASH_FAIL_,Bootlog_Bootrom_img_in_FLASH_OK_,Bootlog_Bootrom_img_in_FLASH_FAIL_,Bootlog_OPER_img_in_prog_OK_,Bootlog_OPER_img_in_prog_FAIL_,Bootlog_Bootrom_img_in_prog_OK_,Bootlog_Bootrom_img_in_prog_FAIL_,Actual_bootlog_0,RED_Bootlog_OPER_img_in_FLASH_OK_,RED_Bootlog_OPER_img_in_FLASH_FAIL_,RED_Bootlog_Bootrom_img_in_FLASH_OK_,RED_Bootlog_Bootrom_img_in_FLASH_FAIL_,RED_Bootlog_OPER_img_in_prog_OK_,RED_Bootlog_OPER_img_in_prog_FAIL_,RED_Bootlog_Bootrom_img_in_prog_OK_,RED_Bootlog_Bootrom_img_in_prog_FAIL_,RED_Bootlog_0,COM_protocol_ver,SW_ver,SW_release,Drv_ver,Drv_release,Active_CPU,Num_COM_task_recv_words,Num_COM_task_send_words,Num_COM_task_fwd_words,Num_COM_task_recv_errors,Num_COM_task_send_errors,Num_COM_task_fwd_errors,Num_RX_fwd_words_CAM,Num_RX_fwd_words_PWR,Num_RX_fwd_words_EXP,Num_RX_error_words_CAM,Num_RX_error_words_PWR,Num_RX_error_words_EXP,Stack_usage_max_[%],Usage,Usage,Usage,Task_-_num_activations_task_0,Task_-_num_activations_task_1,Task_-_num_activations_task_2,Task_-_num_activations_task_3,Task_-_num_activations_task_4,Task_-_num_activations_task_5,Task_-_num_activations_task_6,Task_-_num_activations_task_7,Task_-_num_activations_task_8,Task_-_num_activations_task_9,Task_-_num_activations_task_10,Task_-_num_activations_task_11,Task_-_num_activations_task_12,Task_-_num_activations_task_13,OS_boot_HMC5883_init_OK_,OS_boot_MAG3110_init_OK_,OS_boot_DS3234_init_OK_,OS_boot_MAX21001_init_OK_,OS_boot_FRAM_init_OK_,OS_boot_CPUID_init_OK_,OS_boot_UART_FLUSH_init_OK_,OS_boot_Tasks_create_Error_,OS_boot_Tasks_0_ready_OK_,OS_boot_Sys_header_log_written_to_service_port_,OS_boot_Sys_header_log_silen_on_service_port_,OS_boot_SUN_Sensor_1_init_OK__,OS_boot_SUN_Sensor_2_init_OK__,OS_boot_SUN_Sensor_3_init_OK__,OS_boot_SUN_Sensor_4_init_OK__,OS_boot_SUN_Sensor_5_init_OK__,OS_boot_SUN_Sensor_6_init_OK__,OS_boot_Task_1_init_OK__,OS_boot_Task_2_init_OK__,OS_boot_Task_3_init_OK__,OS_boot_Task_4_init_OK__,OS_boot_Task_5_init_OK__,OS_boot_Task_6_init_OK__,OS_boot_Task_7_init_OK__,OS_boot_Task_8_init_OK__,ERR_storing_Check_activation_time_,ERR_Task_check_not_scheduled_3s_,ERR_Stack_usage_>75%_,ERR_Stack_storing_to_FRAM_fail_,ERR_Gyro_temp_read_fail_,ERR_Gyro_val_read_fail_,ERR_Gyro_val_store_to_FRAM_fail_,ERR_HMC5883_read_fail_,ERR_HMC5883_store_to_FRAM_fail_,ERR_MAG3110_read_fail_,ERR_MAG3110_store_to_FRAM_fail_,ERR_read_ADCS_mode_from_FRAM_fail_,ERR_read_ADCS_K_from_FRAM_fail_,B-dot_executed_,ERR_read_actual_SUN_sensor_fail_,All_SUN_sensors_read_once_in_Check_,Delay_of_Block_7_telemetry,RAD_task_ON/OFF,RAD_RAM_errors_-_Pattern_1,RAD_RAM_errors_-_Pattern_2,RAD_RAM_errors_-_Pattern_3,RAD_RAM_errors_-_Pattern_4,RAD_RAM_errors_-_Pattern_5,RAD_RAM_task_errors,RAD_FRAM_errors_-_Pattern_1,RAD_FRAM_errors_-_Pattern_2,RAD_FRAM_errors_-_Pattern_3,RAD_FRAM_errors_-_Pattern_4,RAD_FRAM_errors_-_Pattern_5,RAD_FRAM_task_errors,RAD_task_-_Flash_Bootrom_image_CRC_wrong,RAD_task_-_Flash_OPER_image_CRC_wrong\n");
                    break;

                case "ADCS":
                    identificator = 2;
                    operation = GetAdcsLine;
                    csvFile.Append("DB_time,ADCS_mode,bd_k,Max_PWM_coil_X,Max_PWM_coil_Y,Max_PWM_coil_Z,SUN_X+_(x),SUN_X+_(y),SUN_X+_irradiation,SUN_X-_(x),SUN_X-_(y),SUN_X-_irradiation,SUN_Y+_(x),SUN_Y+_(y),SUN_Y+_irradiation,SUN_Y-_(x),SUN_Y-_(y),SUN_Y-_irradiation,SUN_Z+_(x),SUN_Z+_(y),SUN_Z+_irradiation,SUN_Z-_(x),SUN_Z-_(y),SUN_Z-_irradiation,Gyroscope_X,Gyroscope_Y,Gyroscope_Z,Gyroscope_temp,Magnetometer_1_X,Magnetometer_1_Y,Magnetometer_1_Z,Magnetometer_1_temp,Magnetometer_2_X,Magnetometer_2_Y,Magnetometer_2_Z,Magnetometer_2_temp,Accelerometer_X,Accelerometer_Y,Accelerometer_Z,Accel_temp,Earth_sensor_X+,Earth_sensor_X-,Earth_sensor_Y+,Earth_sensor_Y-,\n");
                    break;

                case "COM":
                    identificator = 3;
                    operation = GetComLine;
                    csvFile.Append("DB_time,Time:,FW_version:,Active_COM:,Digipeater_mode:,Number_of_reboots:,Output_reflected_power:,Output_forward_power:,Output_reflected_power_CW:,Output_forward_power_CW:,RSSI:,RSSI_noise:,MCU_temperature:,PA_temperature:,CW_beacon_sent:,Packet_beacon_sent:,Correct_packets_received:,Broken_packets_received:,COM_protocol_error:,GS_protocol_error:,TX_disable_stat:,Orbit_time:,Timeslot_start:,Timeslot_end:\n");
                    break;

                case "CAM":
                    identificator = 4;
                    operation = GetCamLine;
                    break;

                case "PWR":
                    identificator = 5;
                    operation = GetPwrLine;
                    csvFile.Append("DB_time,Timestamp,FullUptime,Uptime,Active_procesor,FW_version,Number_of_reboots,CountComErrors,CountPSUErrors,Solar_J_current,Solar_K_current,Solar_L_current,Solar_M_current,Solar_U_current,Solar_D_current,Solar_J_temp,Solar_K_temp,Solar_L_temp,Solar_M_temp,Solar_U_temp,Solar_D_temp,Battery_A_voltage,Battery_B_voltage,Battery_temp,Battery_A_capacity,Battery_B_capacity,Bat_Capacity_(math_0-100%),Battery_A_current,Battery_B_current,Battery_A_min_current,Battery_B_min_current,Battery_A_max_current,Battery_B_max_current,Battery_A_avg_current,Battery_B_avg_current,CHDS_min_current,CHDS_max_current,CHDS_avg_current,CHDS_actual_current,COM_min_current,COM_max_current,COM_avg_current,COM_actual_current,CAM_min_current,CAM_max_current,CAM_avg_current,CAM_actual_current,EXP_min_current,EXP_max_current,EXP_avg_current,EXP_actual_current,ADCS_min_current,ADCS_max_current,ADCS_avg_current,ADCS_actual_current,SYS_voltage_min,SYS_voltage_max,SYS_voltage_actual,Nadprudy_,Stav_pripojenia,TS_Temp,CountPacket,PSU_Error,PSU_Lasterror,COM_error,COM_Lasterror,CDHS_I_limit,COM_I_limit,CAM_I_limit,ADCS_I_limit,EXP_I_limit,CAM_UV_limit,EXP_UV_limit,ADCS_UV_limit,BAT1_Vmax,BAT1_Vmin,BAT2_Vmax,BAT2_Vmin,Endofdata\n");
                    break;

                case "EXP":
                    identificator = 6;
                    operation = GetExpLine;
                    break;

                default:
                    Console.Error.WriteLine("Uknown type of data.");
                    Console.Error.WriteLine("Please provide input file path and type of data you want to parse.");
                    Console.Error.WriteLine("Example: satnogs2csv.exe d:\\satnogs_file.csv COM");
                    return;
            }

            var reader = new StreamReader(args.First());
            string toParse;

            toParse = reader.ReadLine(); int lineNum = 1;
            while (toParse != null)
            {
                string time = Regex.Match(toParse, @"(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})").Groups[1].ToString();
                toParse = Regex.Replace(toParse, @"(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\|)", string.Empty);
                if (toParse.Length > 32)
                {
                    toParse = toParse.Remove(0, 32);
                }

                var id = ushort.Parse(toParse.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
                if (id == identificator)
                {
                    var line = operation(toParse);
                    if (!string.IsNullOrEmpty(line))
                    {
                        csvFile.Append(time + line);
                    }
                }

                toParse = reader.ReadLine(); lineNum++;
            }

            var writer = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "out.csv"));
            writer.Write(csvFile);
            writer.Flush();
        }

        private static string GetExpLine(string toParse)
        {
            List<string> parsed = new List<string>();
            var bajty = new List<byte>();

            for (int i = 2; i < toParse.Length; i = i + 2)
            {
                bajty.Add(byte.Parse(toParse.Substring(i, 2), System.Globalization.NumberStyles.HexNumber));
            }

            if (bajty.Count == 75)
            {
                //// TODO
            }

            return null;
        }

        private static string GetPwrLine(string toParse)
        {
            List<string> parsed = new List<string>();
            var bajty = new List<byte>();

            for (int i = 2; i < toParse.Length; i = i + 2)
            {
                bajty.Add(byte.Parse(toParse.Substring(i, 2), System.Globalization.NumberStyles.HexNumber));
            }
            if (bajty.Count() >= 156)
            {
                var csvLine = ",";
                for (int i = 0; i < 12; i = i + 4)
                {
                    csvLine += BitConverter.ToUInt32(new byte[] { bajty[i], bajty[i + 1], bajty[i + 2], bajty[i + 3] }, 0) + ",";
                }
                for (int i = 12; i < 22; i = i + 2)
                {
                    csvLine += BitConverter.ToUInt16(new byte[] { bajty[i], bajty[i + 1] }, 0) + ", ";
                }
                for (int i = 22; i < 52; i = i + 2)
                {
                    csvLine += BitConverter.ToInt16(new byte[] { bajty[i], bajty[i + 1] }, 0) + ", ";
                }
                for (int i = 52; i < 58; i = i + 2)
                {
                    csvLine += BitConverter.ToUInt16(new byte[] { bajty[i], bajty[i + 1] }, 0) + ", ";
                }
                for (int i = 58; i < 114; i = i + 2)
                {
                    csvLine += BitConverter.ToInt16(new byte[] { bajty[i], bajty[i + 1] }, 0) + ", ";
                }
                for (int i = 114; i < 124; i = i + 2)
                {
                    csvLine += BitConverter.ToUInt16(new byte[] { bajty[i], bajty[i + 1] }, 0) + ", ";
                }
                csvLine += BitConverter.ToInt16(new byte[] { bajty[124], bajty[125] }, 0) + ", ";
                csvLine += BitConverter.ToUInt32(new byte[] { bajty[126], bajty[127], bajty[128], bajty[129] }, 0) + ",";
                for (int i = 130; i < 138; i = i + 2)
                {
                    csvLine += BitConverter.ToUInt16(new byte[] { bajty[i], bajty[i + 1] }, 0) + ", ";
                }
                for (int i = 138; i < 146; i++)
                {
                    csvLine += (ushort)bajty[i] + ",";
                }
                for (int i = 146; i < 154; i = i + 2)
                {
                    csvLine += BitConverter.ToInt16(new byte[] { bajty[i], bajty[i + 1] }, 0) + ", ";
                }
                csvLine += BitConverter.ToUInt16(new byte[] { bajty[154], bajty[155] }, 0) + ", ";

                csvLine += "\n";
                return csvLine;
            }

            return null;
        }

        private static string GetCamLine(string arg)
        {
            throw new NotImplementedException();
        }

        private static string GetAdcsLine(string toParse)
        {
            List<string> parsed = new List<string>();
            var bajty = new List<byte>();

            for (int i = 2; i < toParse.Length; i = i + 2)
            {
                bajty.Add(byte.Parse(toParse.Substring(i, 2), System.Globalization.NumberStyles.HexNumber));
            }
            if (bajty.Count >= 75)
            {
                var csvLine = ",";
                for (int i = 0; i < 5; i++)
                {
                    csvLine += bajty[i] + ",";
                }
                for (int i = 5; i < 35; i = i + 5)
                {
                    csvLine += BitConverter.ToInt16(new byte[] { bajty[i], bajty[i + 1] }, 0) + ", ";
                    csvLine += BitConverter.ToInt16(new byte[] { bajty[i + 2], bajty[i + 3] }, 0) + ", ";
                    csvLine += bajty[i + 4] + ",";
                }
                for (int i = 35; i < 75; i = i + 2)
                {
                    csvLine += BitConverter.ToUInt16(new byte[] { bajty[i], bajty[i + 1] }, 0) + ", ";
                }

                csvLine += "\n";
                return csvLine;
            }
            return null;
        }

        private static string GetCdhsLine(string toParse)
        {
            List<string> parsed = new List<string>();
            var bajty = new List<byte>();

            for (int i = 2; i < toParse.Length; i = i + 2)
            {
                bajty.Add(byte.Parse(toParse.Substring(i, 2), System.Globalization.NumberStyles.HexNumber));
            }

            var bajtyA = bajty.ToArray();

            if (bajty.Count >= 151)
            {
                var csvLine = ",";
                var bity = new BitArray(new byte[] { bajtyA[0] });
                csvLine += (bajtyA[0] & 0xF) + ",";
                csvLine += (bity[5] ? "1" : "0") + ",";
                csvLine += (bity[6] ? "1" : "0") + ",";
                csvLine += (bity[7] ? "1" : "0") + ",";
                csvLine += BitConverter.ToUInt16(bajtyA, 1) + ",";
                csvLine += bajtyA[3] + ",";
                csvLine += bajtyA[4] + ",";
                csvLine += BitConverter.ToUInt32(bajtyA, 5) + ",";
                bity = new BitArray(new byte[] { bajtyA[9] });
                for (int i = 0; i < 8; i++)
                {
                    csvLine += (bity[i] ? "1" : "0") + ",";
                }
                csvLine += bajtyA[10] + ",";
                bity = new BitArray(new byte[] { bajtyA[11] });
                for (int i = 0; i < 8; i++)
                {
                    csvLine += (bity[i] ? "1" : "0") + ",";
                }
                csvLine += bajtyA[12] + ",";
                for (int i = 13; i < 19; i++)
                {
                    csvLine += bajty[i] + ",";
                }
                for (int i = 19; i < 43; i = i + 2)
                {
                    csvLine += BitConverter.ToUInt16(bajtyA, i) + ",";
                }
                for (int i = 51; i < 55; i++)
                {
                    csvLine += bajty[i] + ",";
                }
                for (int i = 55; i < 111; i = i + 4)
                {
                    csvLine += BitConverter.ToUInt32(bajtyA, i) + ",";
                }
                bity = new BitArray(new byte[] { bajtyA[116], bajtyA[117] });
                for (int i = 0; i < 12; i++)
                {
                    if (i != 3)
                    {
                        csvLine += (bity[i] ? "1" : "0") + ",";
                    }
                }
                bity = new BitArray(new byte[] { bajtyA[118], bajtyA[119] });
                for (int i = 0; i < 6; i++)
                {
                    csvLine += (bity[i] ? "1" : "0") + ",";
                }
                for (int i = 8; i < 16; i++)
                {
                    csvLine += (bity[i] ? "1" : "0") + ",";
                }
                bity = new BitArray(new byte[] { bajtyA[120], bajtyA[121] });
                for (int i = 0; i < 16; i++)
                {
                    csvLine += (bity[i] ? "1" : "0") + ",";
                }
                csvLine += BitConverter.ToUInt16(bajtyA, 122) + ",";
                csvLine += bajtyA[124] + ",";
                for (int i = 125; i < 149; i = i + 2)
                {
                    csvLine += BitConverter.ToUInt16(bajtyA, i) + ",";
                }
                csvLine += bajtyA[149] + ",";
                csvLine += bajtyA[150];

                csvLine += "\n";
                return csvLine;
            }

            return null;
        }

        private static string GetComLine(string toParse)
        {
            List<string> parsed = new List<string>();
            var bajty = new List<byte>();

            for (int i = 2; i < toParse.Length; i = i + 2)
            {
                bajty.Add(byte.Parse(toParse.Substring(i, 2), System.Globalization.NumberStyles.HexNumber));
            }
            if (bajty.Count == 40)
            {
                var csvLine = ",";
                csvLine += BitConverter.ToUInt32(new byte[] { bajty[0], bajty[1], bajty[2], bajty[3] }, 0) + ", ";
                csvLine += BitConverter.ToUInt16(new byte[] { bajty[4], bajty[5] }, 0) + ", ";
                csvLine += (ushort)bajty[6] + ",";
                csvLine += (ushort)bajty[7] + ",";
                for (int i = 8; i < 21; i = i + 2)
                {
                    csvLine += BitConverter.ToUInt16(new byte[] { bajty[i], bajty[i + 1] }, 0) + ", ";
                }
                csvLine += unchecked((sbyte)(0 + bajty[22])) + ", "; ////(short)bajty[22] + ", ";
                csvLine += unchecked((sbyte)(0 + bajty[23])) + ", ";
                for (int i = 24; i < 35; i = i + 2)
                {
                    csvLine += BitConverter.ToUInt16(new byte[] { bajty[i], bajty[i + 1] }, 0) + ", ";
                }
                csvLine += (ushort)bajty[36] + ",";
                csvLine += (ushort)bajty[37] + ",";
                csvLine += (ushort)bajty[38] + ",";
                csvLine += (ushort)bajty[39] + "\n";
                return csvLine;
            }
            return null;
        }
    }
}
